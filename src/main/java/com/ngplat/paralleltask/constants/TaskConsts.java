/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.constants
 * @filename: TaskConsts.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.constants;

/** 
 * @typename: TaskConsts
 * @brief: 常量类, 不予构造实例
 * @author: KI ZCQ
 * @date: 2018年5月10日 上午10:35:12
 * @version: 1.0.0
 * @since
 * 
 */
public class TaskConsts {
	
	/**
	 * 常量类, 不允许创建实例.
	 */
	private TaskConsts() {
		throw new IllegalStateException("Utility class.");
	}
	
	/**
	 * 任务图的根节点标识
	 */
	public static final String ROOT_TASK_ID = "ROOT_TASK";
	/**
	 * 默认的任务名称
	 */
	public static final String DEFAULT_TASK_NAME = "Non-Name-Task";
	
	/**
	 * 排序最前
	 */
	public static final int HIGHEST_PRECEDENCE = Integer.MAX_VALUE;
	/**
	 * 排序最后
	 */
	public static final int LOWEST_PRECEDENCE = Integer.MIN_VALUE;
	
	/**
	 * 任务状态：启用, 正常运行
	 */
	public static final String ENABLE_STATUS = "N";
	/**
	 * 任务状态：禁用, 不启动计算, 任务执行状态为(完成)
	 */
	public static final String DISABLE_STATUS = "D";
	
	/**
	 * 任务名称分隔符
	 */
	public static final String TASK_NAME_SEPARATOR = "-";

}
