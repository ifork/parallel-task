/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task
 * @filename: Taskable.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task;

import java.io.Serializable;

/** 
 * @typename: Taskable
 * @brief: 标记接口, 用于集成run接口
 * @author: KI ZCQ
 * @date: 2018年5月15日 上午9:41:55
 * @version: 1.0.0
 * @since
 * 
 */
public interface Taskable extends Runnable, Serializable {
	
	// 实现run接口即可, 处理你的业务逻辑
	
}
