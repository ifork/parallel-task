/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: SpringMain.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ngplat.paralleltask.task.TaskPoolManager;

/** 
 * @typename: SpringMain
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月26日 下午3:00:08
 * @version: 1.0.0
 * @since
 * 
 */
public class SpringMain {

	/**
	 * 创建一个新的实例 SpringMain.
	 */
	public SpringMain() {
	}
	
	public static void main(String[] args) {
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:spring-bean-test.xml");
		// 启动任务
		TaskPoolManager.DEFAULT.start();
		// 关闭上下文
		ctx.close();
	}

}
