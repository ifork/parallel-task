/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: DivTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: DivTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月26日 下午3:46:46
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "4", name = "DivTask")
public class DivTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = 2319095046273513885L;

	/**
	 * 创建一个新的实例 DivTask.
	 */
	public DivTask() {
		// TODO Auto-generated constructor stub
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		System.out.println(String.format("[DivTask] runWorker, result: %d", (10 / 5)));
	}

}
